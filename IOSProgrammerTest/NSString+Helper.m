//
//  UIView+Manipulate.m
//  IOSProgrammerTest
//
//  Created by Xin Sun on 15/8/25.
//  Copyright (c) 2015年 AppPartner. All rights reserved.
//

#import "NSString+Helper.h"

@implementation NSString (Helper)

-(NSString *)URLEncode
{
    return CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(__bridge CFStringRef)self,NULL,(CFStringRef)@"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8));
}

@end