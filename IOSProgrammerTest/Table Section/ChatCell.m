//
//  TableSectionTableViewCell.m
//  IOSProgrammerTest
//
//  Created by Justin LeClair on 12/15/14.
//  Copyright (c) 2014 AppPartner. All rights reserved.
//

#import "ChatCell.h"
#import "UIImageView+AFNetworking.h"
#import "UIView+Manipulate.h"

@interface ChatCell ()
@property (nonatomic, strong) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *msgLabel;
@end

@implementation ChatCell

- (void)awakeFromNib
{
    // Initialization code
    [self.usernameLabel setFont:[UIFont fontWithName:@"Machinato" size:18]];
    [self.msgLabel setFont:[UIFont fontWithName:@"Machinato" size:15]];
    
    self.usernameLabel.textColor = [UIColor colorWithRed:37/255.0 green:37/255.0 blue:37/255.0 alpha:1.0];
    self.usernameLabel.textColor = [UIColor colorWithRed:17/255.0 green:17/255.0 blue:17/255.0 alpha:1.0];
    
    [self.profilePic.layer setCornerRadius:18.0f];
    [self.profilePic.layer setMasksToBounds:YES];
}

- (void)loadWithData:(ChatData *)chatData
{
    self.usernameLabel.text = chatData.username;
    self.msgLabel.text = chatData.message;
    
    [self.profilePic setContentMode:UIViewContentModeScaleAspectFill];
    [self.profilePic setImageWithURL:[NSURL URLWithString:chatData.avatar_url] placeholderImage:[UIImage imageNamed:@"logo"] success:nil]; // loading image using Operation Queue, show logo as placeholder image first
}
@end
