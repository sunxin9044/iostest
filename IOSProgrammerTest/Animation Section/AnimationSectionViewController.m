//
//  AnimationSectionViewController.m
//  IOSProgrammerTest
//
//  Created by Justin LeClair on 12/15/14.
//  Copyright (c) 2014 AppPartner. All rights reserved.
//

#import "AnimationSectionViewController.h"
#import "MainMenuViewController.h"

@interface AnimationSectionViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (nonatomic) double angle;
@end

@implementation AnimationSectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // Set Navigation title
    self.navigationItem.title = @"Animation";
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Machinato" size:20], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    
    [self setUpBackBtn]; // Set up back button
    
    // add drag gesture
    UILongPressGestureRecognizer * gesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(doHandlePanAction:)];
    [self.logo addGestureRecognizer:gesture];
    [self.logo setUserInteractionEnabled:YES];
}

- (void)setUpBackBtn {
    UIImage *buttonImage = [UIImage imageNamed:@"back_icon.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

// long click to drag
- (void)doHandlePanAction:(UILongPressGestureRecognizer *)gesture {
    
    if(gesture.state != UIGestureRecognizerStateEnded) {
        CGPoint point = [gesture locationInView:self.view];
        gesture.view.center = CGPointMake(point.x, point.y);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)spin:(id)sender {
    CGAffineTransform transform = CGAffineTransformMakeRotation((_angle * M_PI)/180);
    [UIView animateWithDuration:0.01f animations:^{
        self.logo.transform = transform;
    } completion:^(BOOL finished) {
        if (_angle < 360) {
            _angle+=10;
            [self spin:sender];
        } else {
            self.logo.transform = CGAffineTransformIdentity;
            _angle = 0;
        }
    }];
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
