//
//  UIView+Manipulate.m
//  IOSProgrammerTest
//
//  Created by Xin Sun on 15/8/25.
//  Copyright (c) 2015年 AppPartner. All rights reserved.
//

#import "UIView+Manipulate.h"

@implementation UIView (Manipulate)

/*Change UIView with rounded border*/
- (void)roundedCornerWithRadius:(CGFloat)r andBorderSize:(CGFloat)size andColor:(UIColor *)color {
    [self.layer setCornerRadius:r];
    if (size > 0) {
        [self.layer setBorderColor:[color CGColor]];
        [self.layer setBorderWidth:size];
    }
}

@end
