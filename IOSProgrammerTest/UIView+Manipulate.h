//
//  UIView+Manipulate.h
//  IOSProgrammerTest
//
//  Created by Xin Sun on 15/8/25.
//  Copyright (c) 2015年 AppPartner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Manipulate)
- (void)roundedCornerWithRadius:(CGFloat)r andBorderSize:(CGFloat)size andColor:(UIColor *)color ;
@end
