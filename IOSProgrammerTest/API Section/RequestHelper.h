//
//  UIView+Manipulate.h
//  IOSProgrammerTest
//
//  Created by Xin Sun on 15/8/25.
//  Copyright (c) 2015年 AppPartner. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^RequestHandler)(NSMutableDictionary*);

@interface RequestHelper : NSObject

+ (void)loginWithLoginData:(NSDictionary *)loginData onCompletion:(RequestHandler)complete;

@end
