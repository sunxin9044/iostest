//
//  UIView+Manipulate.m
//  IOSProgrammerTest
//
//  Created by Xin Sun on 15/8/25.
//  Copyright (c) 2015年 AppPartner. All rights reserved.
//

#import "RequestHelper.h"
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"

@implementation RequestHelper

/**
 * Login Function
 * type: POST
 * parms: username(string), password(string)
 * return: login response from server
 * returnType: JSON
 */
+ (void)loginWithLoginData:(NSDictionary *)loginData
              onCompletion:(RequestHandler)complete {
    NSString *detailPage = @"AppPartnerProgrammerTest/scripts/login.php";
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://dev.apppartner.com"]];
    [httpClient setParameterEncoding:AFFormURLParameterEncoding]; // add encoding type
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:detailPage
                                                      parameters:loginData];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *e = nil;
        NSMutableDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: &e];
        
        if (complete)
            complete(jsonArray);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error); // handle error
    }];
    [operation start];
}

@end
