//
//  APISectionViewController.m
//  IOSProgrammerTest
//
//  Created by Justin LeClair on 12/15/14.
//  Copyright (c) 2014 AppPartner. All rights reserved.
//

#import "LoginSectionViewController.h"
#import "MainMenuViewController.h"
#import "RequestHelper.h"
#import "NSString+Helper.h"

@interface LoginSectionViewController () <UIAlertViewDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (nonatomic, strong) NSMutableDictionary *loginData; // should build an object
@end

@implementation LoginSectionViewController

// Init dictionary
- (NSMutableDictionary *)loginData {
    if (!_loginData) {
        _loginData = [[NSMutableDictionary alloc] init];
    }
    
    return _loginData;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // Set Navigation title
    self.navigationItem.title = @"Login";
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Machinato" size:20], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    
    [self setUpBackBtn]; // Set up back button
    
    // Change placeholder color if above iOS 7
    if ([self.usernameTextField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor colorWithRed:137/255.0 green:137/255.0 blue:137/255.0 alpha:1.0];
        self.usernameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"username" attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:@"Machinato" size:18]}];
    }
    if ([self.passwordTextField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor colorWithRed:137/255.0 green:137/255.0 blue:137/255.0 alpha:1.0];
        self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"password" attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:@"Machinato" size:18]}];
    }
    
    self.usernameTextField.delegate = self;
    self.passwordTextField.delegate = self;
}

- (void)setUpBackBtn {
    UIImage *buttonImage = [UIImage imageNamed:@"back_icon.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)login:(id)sender {
    [self.usernameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    NSString *username = self.usernameTextField.text;
    username = [username URLEncode];
    NSString *password = self.passwordTextField.text;
    password = [password URLEncode];
    
    // Check input valid
    if (username.length == 0 || password.length == 0) {
        [self createAlertFor:@"Oops!" withContent:@"Username and Password cannot be empty." andBtn:@"GOT IT!"];
    } else {
        [self.loginData setObject:username forKey:@"username"];
        [self.loginData setObject:password forKey:@"password"];
        
        double t = [[NSDate date] timeIntervalSince1970];
        [RequestHelper loginWithLoginData:self.loginData onCompletion:^(NSMutableDictionary *result) {
            double r = [[NSDate date] timeIntervalSince1970];
            if ([result[@"code"] isEqualToString:@"Error"]) {
                [self createAlertFor:result[@"code"] withContent:[NSString stringWithFormat:@"%@, cost:%.2fms",result[@"message"],(r-t)*1000] andBtn:@"GOT IT!"];
            } else {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:result[@"code"]
                                                               message:[NSString stringWithFormat:@"%@, cost:%.2fms",result[@"message"],(r-t)*1000]
                                                              delegate:self
                                                     cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
        }];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction)backAction:(id)sender
{
    [self.usernameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)createAlertFor:(NSString *)title withContent:(NSString *)content andBtn:(NSString *)btn {
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = title;
    alert.message = content;
    [alert addButtonWithTitle:btn];
    [alert show];
}

@end
