//
//  ViewController.m
//  IOSProgrammerTest
//
//  Created by Justin LeClair on 12/15/14.
//  Copyright (c) 2014 AppPartner. All rights reserved.
//

#import "MainMenuViewController.h"
#import "ChatSectionViewController.h"
#import "LoginSectionViewController.h"
#import "AnimationSectionViewController.h"

/* Added by Xin*/
#import "UIView+Manipulate.h"

@interface MainMenuViewController ()
@property (weak, nonatomic) IBOutlet UIView *chatBtnView;
@property (weak, nonatomic) IBOutlet UIView *loginBtnView;
@property (weak, nonatomic) IBOutlet UIView *animationBtnView;
@property (weak, nonatomic) IBOutlet UILabel *chatTitle;
@property (weak, nonatomic) IBOutlet UILabel *loginLabel;
@property (weak, nonatomic) IBOutlet UILabel *animationLabel;

@end

@implementation MainMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.chatBtnView roundedCornerWithRadius:25.0f andBorderSize:0 andColor:nil];
    [self.loginBtnView roundedCornerWithRadius:25.0f andBorderSize:0 andColor:nil];
    [self.animationBtnView roundedCornerWithRadius:25.0f andBorderSize:0 andColor:nil];
    
    [self.chatTitle setFont:[UIFont fontWithName:@"Machinato" size:22]];
    [self.loginLabel setFont:[UIFont fontWithName:@"Machinato" size:22]];
    [self.animationLabel setFont:[UIFont fontWithName:@"Machinato" size:22]];
    
    self.navigationItem.title = @"Coding Task";
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Machinato" size:20], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tableSectionAction:(id)sender
{
    ChatSectionViewController *tableSectionViewController = [[ChatSectionViewController alloc] init];
    [self.navigationController pushViewController:tableSectionViewController animated:YES];
}
- (IBAction)apiSectionAction:(id)sender
{
    LoginSectionViewController *apiSectionViewController = [[LoginSectionViewController alloc] init];
    [self.navigationController pushViewController:apiSectionViewController animated:YES];
}
- (IBAction)animationSectionAction:(id)sender
{
    AnimationSectionViewController *animationSectionViewController = [[AnimationSectionViewController alloc] init];
    [self.navigationController pushViewController:animationSectionViewController animated:YES];
}
@end
